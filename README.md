![](https://puu.sh/Grjvw/a264045056.png)
# Surf
Simple tool for people who like to play on several private servers for osu! but don't want the hassle of having a server switcher for each server. Example configs for **Akatsuki** and **Ripple** are included in the [examples folder](https://github.com/Shoot/Surf/tree/master/examples).

## Requirements
* .NET Framework 4.8
* Newtonsoft.Json 12.0.3
* WindowsAPICodePack 1.1.1

## Configuration
### User
All user configuration is stored within `%APPDATA%\SurfOsu\settings.json`. Currently this will include the following.
```json
{
	"Server": null,
	"GamePath": "D:\\Games\\osu!"
}
```
Where the `Server` key holds the name of the currently loaded configuration file, and `GamePath` holds the path of osu! as set by the user within the program.

### Server
#### Dynamic example
```json
{
	"name": "osu! private server",
	"dynamic": true,
	"url": "https://example.org/osu/hosts.json"
}
```
#### Static example
```json
{
	"name": "osu! private server",
	"dynamic": false,
	"ips": {
		"osu.ppy.sh": "123.456.789.012",
		"c.ppy.sh": "123.456.789.012",
		"c4.ppy.sh": "123.456.789.012",
		"c5.ppy.sh": "123.456.789.012",
		"c6.ppy.sh": "123.456.789.012",
		"ce.ppy.sh": "123.456.789.012",
		"a.ppy.sh": "123.456.789.012",
		"s.ppy.sh": "123.456.789.012",
		"i.ppy.sh": "123.456.789.012"
	}
}
```
#### `name`
The `name` parameter holds the name of the private server as a string, shown to the user within the combobox and on the "current server" status only. It is not used for uniquely identifying configuration files, as the configuration's file name is used instead.
#### `dynamic`
Whether to get hosts from the `url` key, or to get them from the `ips` dictionary.
#### `url`
URL that returns hosts as a JSON dictionary.
#### `ips`
IPs to use for the hosts as a dictionary.

For both the `url` and `ips` parameters each key will be looped over, and if it ends in `.ppy.sh` it will have its value set in the hosts file. Otherwise, it will be skipped. This is done to avoid the server setting malicious values in the hosts file.