﻿using System;
using System.IO;
using System.Media;
using System.Windows;
using System.Net.Http;
using System.Diagnostics;
using System.Collections.Generic;
using Microsoft.WindowsAPICodePack.Dialogs;
using Newtonsoft.Json;

namespace Surf
{
	public partial class MainWindow : Window
	{
		string current_directory = Directory.GetCurrentDirectory();
		List<Server> servers = new List<Server>();
		Config user_config;
		HttpClient client = new HttpClient();
		public MainWindow()
		{
			InitializeComponent();

			user_config = Utilities.LoadConfig();

			Launch.IsEnabled = Utilities.ValidateGamePath(user_config.GamePath);

			string server_folder = Path.Combine(current_directory, "servers");

			if (!Directory.Exists(server_folder))
			{
				System.Windows.MessageBox.Show("No 'servers' folder was detected!");
				System.Windows.Application.Current.Shutdown();
				return;
			}

			string[] file_paths = Directory.GetFiles(server_folder);

			Server default_server = new Server { Name = "Bancho", Dynamic = false, URL = "", Bancho = true };

			servers.Add(default_server);
			ServerList.Items.Add(default_server.Name);
			ServerList.SelectedIndex = 0;

			ServerStatus.Text = Utilities.PingServer(default_server);

			for (int i = 0; i < file_paths.Length; i++)
			{
				string path = file_paths[i];
				string file = Path.GetFileName(path);

				if (!file.EndsWith(".json"))
					continue;

				string content = File.ReadAllText(path);
				Server server = JsonConvert.DeserializeObject<Server>(content);

				if (file == user_config.Server)
				{
					ServerList.SelectedIndex = i + 1;
					CurrentServer.Text = server.Name;
				}

				server.Path = file;

				servers.Add(server);
				ServerList.Items.Add(server.Name);
			}

			if (servers.Count < 1)
			{
				System.Windows.MessageBox.Show("No servers could be found!");
				System.Windows.Application.Current.Shutdown();
				return;
			}
		}
		private async void Load_Click(object sender, RoutedEventArgs e)
		{
			Status.Text = "Changing server...";
			Server server = servers[ServerList.SelectedIndex];

			string hosts_path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.System), "drivers/etc/hosts");

			List<string> new_lines = new List<string>();

			foreach (string line in File.ReadAllLines(hosts_path))
			{
				string[] data = line.Split('\t');

				if ((data.Length == 2 && !data[1].EndsWith(".ppy.sh")) || data.Length < 2)
					new_lines.Add(line);
			}

			if (server == servers[0])
				goto Finish;

			bool failed = false;
			var hosts = new Dictionary<string, string>();

			if (server.Dynamic)
			{
				try
				{
					var result = await client.GetAsync(server.URL);
					hosts = JsonConvert.DeserializeObject<Dictionary<string, string>>(await result.Content.ReadAsStringAsync());
				}
				catch (Newtonsoft.Json.JsonReaderException)
				{
					System.Windows.MessageBox.Show(String.Format("Failed to read JSON value for '{0}'!", server.Name));
					failed = true;
				}
				catch (HttpRequestException)
				{
					System.Windows.MessageBox.Show(String.Format("Unknown HTTP error occured for '{0}'!", server.Name));
					failed = true;
				}
			}
			else
			{
				hosts = server.IPs;
			}

			if (failed)
			{
				ServerStatus.Text = "Failed";
				Status.Text = "Idle";

				return;
			}

			foreach (var key in hosts.Keys)
			{
				if (key.EndsWith(".ppy.sh"))
					new_lines.Add(String.Format("{0}\t{1}", hosts[key], key));
			}

		Finish:
			CurrentServer.Text = server.Name;
			Status.Text = "Idle";
			ServerStatus.Text = Utilities.PingServer(server);

			user_config.Server = server.Path;
			Utilities.SaveConfig(user_config);

			File.WriteAllLines(hosts_path, new_lines);

			SystemSounds.Exclamation.Play();
			System.Windows.MessageBox.Show("Updated server successfully!");
		}
		private void Ping_Click(object sender, RoutedEventArgs e)
		{
			ServerStatus.Text = "Pinging...";
			ServerStatus.Text = Utilities.PingServer(servers[ServerList.SelectedIndex]);
		}
		private void Launch_Click(object sender, RoutedEventArgs e)
		{
			if (!Utilities.ValidateGamePath(user_config.GamePath))
				return;

			Process.Start(Path.Combine(user_config.GamePath, "osu!.exe"));
			System.Windows.Application.Current.Shutdown();
		}
		private void OpenGamePath_Click(object sender, RoutedEventArgs e)
		{
			var dialog = new CommonOpenFileDialog { IsFolderPicker = true };

			if (dialog.ShowDialog() == CommonFileDialogResult.Ok)
			{
				if (Utilities.ValidateGamePath(dialog.FileName))
				{
					Launch.IsEnabled = true;
					user_config.GamePath = dialog.FileName;

					Utilities.SaveConfig(user_config);
				}
				else
				{
					Launch.IsEnabled = false;

					SystemSounds.Exclamation.Play();
					System.Windows.MessageBox.Show("osu!.exe could not be detected in the given path.");
				}
			}
		}
	}
}
