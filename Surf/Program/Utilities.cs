﻿using System;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using Newtonsoft.Json;

namespace Surf
{
	class Utilities
	{
		public static void SaveConfig(Config config)
		{
			var fresh_config = new Config { };

			var app_data = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
			var program_folder = Path.Combine(app_data, "SurfOsu");

			if (!Directory.Exists(program_folder))
				Directory.CreateDirectory(program_folder);

			var filename = Path.Combine(app_data, "SurfOsu", "settings.json");
			File.WriteAllText(filename, JsonConvert.SerializeObject(config));
		}
		public static Config LoadConfig()
		{
			var fresh_config = new Config { };

			var app_data = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
			var program_folder = Path.Combine(app_data, "SurfOsu");

			Directory.CreateDirectory(program_folder);

			var filename = Path.Combine(app_data, "SurfOsu", "settings.json");

			if (!File.Exists(filename))
				return fresh_config;

			string text = System.IO.File.ReadAllText(filename);

			try
			{
				return JsonConvert.DeserializeObject<Config>(text);
			}
			catch (Newtonsoft.Json.JsonReaderException)
			{
				return fresh_config;
			}
		}
		public static bool ValidateGamePath(string game_path)
		{
			if (Directory.Exists(game_path))
			{
				string[] files = Directory.GetFiles(game_path);

				foreach (string file in files)
				{
					if (Path.GetFileName(file) == "osu!.exe")
						return true;
				}
			}

			return false;
		}
		public static string PingServer(Server server)
		{
			string ping_result = string.Empty;
			var pinger = new Ping();

			IPAddress[] addresses = Dns.GetHostAddresses("osu.ppy.sh");

			try
			{
				PingReply reply = pinger.Send(addresses[0]);
				ping_result = String.Format("Online ({0}ms)", reply.RoundtripTime);
			}
			catch (PingException)
			{
				ping_result = "Offline";
			}
			finally
			{
				pinger.Dispose();
			}

			return ping_result;
		}
	}
}
