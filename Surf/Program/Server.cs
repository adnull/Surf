﻿using System.Collections.Generic;

namespace Surf
{
	class Server
	{
		public string Name { get; set; }
		public bool Dynamic { get; set; }
		public string URL { get; set; }
		public Dictionary<string, string> IPs { get; set; }
		public string Path { get; set; }
		public bool Bancho { get; set; } = false;
	}
}
